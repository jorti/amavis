# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::ZMQTest;

use v5.20;
use Test::Most;
use base 'Test::Class';

use ZMQ::LibZMQ3;
use ZMQ::Constants qw(:all);

sub class { 'Amavis::ZMQ' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub messages : Tests(4) {
  my $class = shift->class;

  my $sockname = 'tcp://127.0.0.1:9999';
  my $teststring = 'ZMQ_Test_String';
  my ($server_ctx, $server_sock);
  if (eval {
    $server_ctx  = zmq_init(1) or die $!;
    $server_sock = zmq_socket( $server_ctx, &ZMQ_SUB);
    zmq_setsockopt($server_sock, &ZMQ_SUBSCRIBE, '') != -1 or die $!;
    1;
  }){
    my $pid = open (my $from_child, '-|');
    if (defined $pid and $pid) {
      ok 1, 'test forked';
    }
    return unless defined($pid);
    if ($pid) {
      zmq_bind($server_sock, $sockname);
      is <$from_child>, 'client initialized', 'client initialized';
      zmq_poll([{
          socket => $server_sock,
          events => &ZMQ_POLLIN,
          callback => sub {
            ok  zmq_msg_data(zmq_recvmsg($server_sock)) =~ /$teststring/,
              'server received teststring';
          }
        }],
        -1,
      );
      ok waitpid($pid, 0), 'client terminated';
    } else {
      my $zmq = $class->new($sockname);
      print 'client initialized';
      close STDOUT;
      sleep 1;
      $zmq->register_proc(1, 0, $teststring);
      exit;
    }
  } else {
    is $@, undef, 'ZMQ initialized';
  }
}

1;
