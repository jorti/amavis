# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::LogTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Log' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  return 'Incomplete class';
  use_ok $test->class;
}

sub empty : Tests(0) {
}

1;
