# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::rfc2821_2822_ToolsTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::rfc2821_2822_Tools' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub unfold {
  $_[0] =~ s/\n(?=[ \t])//gsr;
}

sub folding : Tests(4) {
  my $test  = shift;

  unlike(
    wrap_string('0000 00000' x 10, undef, undef, undef, 1),
    qr/\n/,
    'structured: wrap_string does not fold');

  my ($orig, $back_and_forth);

  $orig = '0000 00000' x 10;
  $back_and_forth = unfold(wrap_string($orig, undef, undef, undef, 0));
  is($back_and_forth, $orig, 'unstructured: wrap_string is reversible');

  $orig = "0000\n00000" x 10;
  $back_and_forth = unfold(wrap_string($orig, undef, undef, undef, 0));
  unlike($back_and_forth, qr/\n(?![\t ])/, 'unstructured: wrap_string does not return multiple lines that could be mistaken for multiple headers');

  $orig = "0000\n00000" x 10;
  $back_and_forth = unfold(wrap_string($orig, undef, undef, undef, 1));
  unlike($back_and_forth, qr/\n(?![\t ])/, 'structured: wrap_string does not return multiple lines that could be mistaken for multiple headers');
}

1;
